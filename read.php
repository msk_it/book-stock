<?php
	session_start();
    require_once "dbconnection.php";

	$id = $_GET['id'];
    
    // Prepare a select statement
    $sql = "SELECT b.Name AS Name, a.Name AS Author, b.Year AS Year, b.Price AS Price, b.ISBN AS ISBN, b.Medium AS Medium, g.Name AS Category FROM books b, author a, category g WHERE b.ISBN = '$id' AND b.Category = g.id AND b.Author = a.id";
    
    if($result = mysqli_query($db, $sql)){
       
            if(mysqli_num_rows($result) == 1){
                $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
                $name = $row["Name"];
                $author = $row["Author"];
                $year = $row["Year"];
				$price = $row["Price"];
				$isbn = $row["ISBN"];
                $medium = $row["Medium"];
				$category = $row["Category"];
            }
				else "<p class='lead'><em>No records were found.</em></p>";
            
			} else{
            	echo "ERROR: Could not able to execute $sql. " . mysqli_error($db);
			}
    
    mysqli_close($db);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>View Record</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.css">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.js"></script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
<link href="style.css" rel="stylesheet" type="text/css">

       <style type="text/css">
        .wrapper{
            width: 400px;
            margin: 0 auto;
			font-size: 16px;
			border: solid;
			border-color: #007bff;
        }
    </style>
</head>
<body>
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header">
                        <h1>View Record</h1>
                    </div>
                    <div class="form-group">
                        <label>Title : <?php echo $name; ?></label>
                    </div>
                    <div class="form-group">
                        <label>Author : <?php echo $author; ?></label>
                    </div>
                    <div class="form-group">
						<label>Year : <?php echo $year; ?></label>
                    </div>
					<div class="form-group">
                        <label>Price : <?php echo $price; ?></label>
                    </div>
					<div class="form-group">
						<label>ISBN : <?php echo $isbn; ?></label>
                    </div>
					<div class="form-group">
                        <label>Medium : <?php echo $medium; ?></label>
                    </div>
					<div class="form-group">
                        <label>Category : <?php echo $category; ?></label>
                    </div>
                    <p><a href="main.php" class="btn btn-primary">Back</a></p>
                </div>
            </div>        
        </div>
    </div>
</body>
</html>