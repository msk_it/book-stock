<?php

include_once 'dbconnection.php';
if ( !empty( $_POST[ "search_id" ] ) ) {
	$pr = $_POST[ "search_id" ];
	echo "<script> console.log('value of ajax" . $pr . "')</script>";

	$query = "SELECT Name, Author, Price,ISBN, Image FROM books WHERE Name LIKE '" . $_POST[ 'search_id' ] . "%'";
	$result = $db->query( $query );


	if ( $result->num_rows > 0 ) {
		echo "<table id='table1' class='table table-bordered table-striped'>";
		echo "<thead>";
		echo "<tr>";
		echo "<th>Title</th>";
		echo "<th>Author</th>";
		echo "<th>Price</th>";
		echo "<th>Image</th>";
		echo "<th>Action</th>";
		echo "</tr>";
		echo "</thead>";
		echo "<tbody>";
		while ( $row = $result->fetch_assoc() ) {
			echo "<tr>";
			echo "<td>" . $row[ 'Name' ] . "</td>";
			echo "<td>" . $row[ 'Author' ] . "</td>";
			echo "<td>" . $row[ 'Price' ] . "</td>";
			if ( $row[ 'Image' ] == null ) {
				echo "<td> <img src ='images/default.png' style='width:40px; height:60px'/></td>";
			}
			if ( $row[ 'Image' ] != null ) {
				echo "<td> <img src ='images/" . $row[ 'Image' ] . "' style='width:40px; height:60px'/></td>";
			}
			echo "<td>";
			echo "<a href='read.php?id=" . $row[ 'ISBN' ] . "' title='View Record' data-toggle='tooltip'><i class='fas fa-eye'></i></a>";
			echo "<a href='update.php?id=" . $row[ 'ISBN' ] . "' title='Update Record' data-toggle='tooltip'><i class='fas fa-edit'></i></a>";
			echo "<a href='delete.php?id=" . $row[ 'ISBN' ] . "' title='Delete Record' data-toggle='tooltip'><i class='fas fa-trash'></i></a>";
			echo "</td>";
			echo "</tr>";
		}
		echo "</tbody>";
	} else {
		echo '<option value="">No Result</option>';
	}
}

if ( !empty( $_POST[ "category_id" ] ) ) {
	$pr = $_POST[ "category_id" ];
	echo "<script> console.log('value of ajax" . $pr . "')</script>";

	$query = "SELECT b.Name AS Name, b.Author AS Author, b.Price AS Price, b.ISBN AS ISBN, b.Image AS Image FROM books b, category g WHERE b.Category = g.id AND g.id = '" . $_POST[ 'category_id' ] . "%'";
	$result = $db->query( $query );


	if ( $result->num_rows > 0 ) {
		echo "<table id='table1' class='table table-bordered table-striped'>";
		echo "<thead>";
		echo "<tr>";
		echo "<th>Title</th>";
		echo "<th>Author</th>";
		echo "<th>Price</th>";
		echo "<th>Image</th>";
		echo "<th>Action</th>";
		echo "</tr>";
		echo "</thead>";
		echo "<tbody>";
		while ( $row = $result->fetch_assoc() ) {
			echo "<tr>";
			echo "<td>" . $row[ 'Name' ] . "</td>";
			echo "<td>" . $row[ 'Author' ] . "</td>";
			echo "<td>" . $row[ 'Price' ] . "</td>";
			if ( $row[ 'Image' ] == null ) {
				echo "<td> <img src ='images/default.png' style='width:40px; height:60px'/></td>";
			}
			if ( $row[ 'Image' ] != null ) {
				echo "<td> <img src ='images/" . $row[ 'Image' ] . "' style='width:40px; height:60px'/></td>";
			}
			echo "<td>";
			echo "<a href='read.php?id=" . $row[ 'ISBN' ] . "' title='View Record' data-toggle='tooltip'><i class='fas fa-eye'></i></a>";
			echo "<a href='update.php?id=" . $row[ 'ISBN' ] . "' title='Update Record' data-toggle='tooltip'><i class='fas fa-edit'></i></a>";
			echo "<a href='delete.php?id=" . $row[ 'ISBN' ] . "' title='Delete Record' data-toggle='tooltip'><i class='fas fa-trash'></i></a>";
			echo "</td>";
			echo "</tr>";
		}
		echo "</tbody>";
	} else {
		echo '<option value="">No Result</option>';
	}
}
?>