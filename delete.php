<?php
session_start();
if ( $_SERVER["REQUEST_METHOD"] == "POST") {
	
	require_once "dbconnection.php";

	$id = $_POST['id'];

	
	$sql = "DELETE FROM books WHERE ISBN = '$id'";

	
	if ( mysqli_query( $db, $sql ) ) {
		
		header( "location: main.php" );
		exit();
	} else {
		echo "Oops! Something went wrong. Please try again later.";
	}

	
	mysqli_close( $db );
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Delete Record</title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.css">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.js"></script>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
	<link href="style.css" rel="stylesheet" type="text/css">
	<style type="text/css">
		.wrapper {
			width: 500px;
			margin: 0 auto;
		}

	</style>
</head>

<body>
	<div class="wrapper">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="page-header">
						<h1>Delete Record</h1>
					</div>
					<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
						<div class="alert alert-danger" role="alert">
							<input type="hidden" name="id" value="<?php echo trim($_GET["id"]); ?>"/>
							<p>Are you sure you want to delete this book?</p><br>
							<p>
								<input type="submit" value="Yes" class="btn btn-danger">
								<a href="main.php" class="btn btn-default">No</a>
							</p>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>