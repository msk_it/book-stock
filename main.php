<?php
include( 'session.php' );
?>
<html>
<head>
	<title>Dashbord</title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.css">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.js"></script>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
	<link href="style.css" rel="stylesheet" type="text/css">

	<script>
		$( document ).ready( function () {
			$( '#search' ).on( 'input', function () {
				var searchID = $( this ).val();
				if ( searchID ) {
					$.ajax( {
						type: 'POST',
						url: 'ajaxData.php',
						data: 'search_id=' + searchID,
						success: function ( html ) {
							$( '#table1' ).html( html );
						}
					} );
					console.log( "valuve of " + searchID );
				} else {

				}
			} );
		} );

		$( document ).ready( function () {
			$( '#category' ).on( 'change', function () {
				var categoryID = $( this ).val();
				if ( categoryID ) {
					$.ajax( {
						type: 'POST',
						url: 'ajaxData.php',
						data: 'category_id=' + categoryID,
						success: function ( html ) {
							$( '#table1' ).html( html );
						}
					} );
					console.log( "valuve of " + categoryID );
				} else {

				}
			} );
		} );
		$( '[data-toggle="tooltip"]' ).tooltip();
	</script>

</head>

<body>
	<div class="wrapper">
		<div class="container-fluid">
		<h2>Welcome <?php echo $login_session; ?></h2> 
      	<h4><a href = "logout.php">Sign Out</a></h4>
		</div>
		<div style="border: solid; border-color: #007bff" class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="page-header clearfix">
						<h1 class="pull-left">Book Details</h1>
						<div class="row m-2">
							<div class="col-sm">
								<a href="create.php" class="btn btn-success mr-2">Add New Book</a>
							</div>
							<div class="col-sm">
								<input type="text" id="search" style="width: 150px" placeholder="Search Books" class="form-control">
							</div>
							<div class="col-sm">
								<select style="width: 150px" id="category" class="form-control" name="category" required>
									<?php
									include_once 'dbconnection.php';
									$sql = "SELECT * from category";
									$result = $db->query( $sql );
									if ( $result->num_rows > 0 ) {
										while ( $row = $result->fetch_assoc() ) {
											echo "<option value='" . $row[ 'id' ] . "'>" . $row[ 'Name' ] . "</option>";
										}
									}
									?>
								</select>
							</div>
						</div>
					</div>
					<table id="table1" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>Title</th>
								<th>Author</th>
								<th>Price</th>
								<th>Image</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php
							
							require_once "dbconnection.php";

							
							$sql = "SELECT b.Name AS Name, a.Name AS Author, b.ISBN AS ISBN, b.Price AS Price, b.Image AS Image FROM books b, author a WHERE b.Author = a.id";
							if ( $result = mysqli_query( $db, $sql ) ) {
								if ( mysqli_num_rows( $result ) > 0 ) {
									while ( $row = mysqli_fetch_array( $result ) ) {
										echo "<tr>";
										echo "<td>" . $row[ 'Name' ] . "</td>";
										echo "<td>" . $row[ 'Author' ] . "</td>";
										echo "<td>" . $row[ 'Price' ] . "</td>";
										if ( $row[ 'Image' ] == null ) {
											echo "<td> <img src ='images/default.png' style='width:40px; height:60px'/></td>";
										}
										if ( $row[ 'Image' ] != null ) {
											echo "<td> <img src ='images/" . $row[ 'Image' ] . "' style='width:40px; height:60px'/></td>";
										}
										echo "<td>";
										echo "<a href='read.php?id=" . $row[ 'ISBN' ] . "' title='View Record' data-toggle='tooltip'><i class='fas fa-eye'></i></a>";
										echo "<a href='update.php?id=" . $row[ 'ISBN' ] . "' title='Update Record' data-toggle='tooltip'><i class='fas fa-edit'></i></a>";
										echo "<a href='delete.php?id=" . $row[ 'ISBN' ] . "' title='Delete Record' data-toggle='tooltip'><i class='fas fa-trash'></i></a>";
										echo "</td>";
										echo "</tr>";
									}
									
									mysqli_free_result( $result );
								} else {
									echo "<p class='lead'><em>No records were found.</em></p>";
								}
							} else {
								echo "ERROR: Could not able to execute $sql. " . mysqli_error( $db );
							}

							
							mysqli_close( $db );
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</body>
</html>