<?php
include( "dbconnection.php" );
session_start();

$error = "";

if ( $_SERVER[ "REQUEST_METHOD" ] == "POST" ) {

	$myusername = mysqli_real_escape_string( $db, $_POST[ 'username' ] );
	$mypassword = mysqli_real_escape_string( $db, $_POST[ 'password' ] );

	$sql = "SELECT id FROM user WHERE username = '$myusername' and password = '$mypassword'";
	$result = mysqli_query($db,$sql);
	
	$row = mysqli_fetch_array($result);

	$count = mysqli_num_rows($result );

	if ( $count == 1 ) {
		$_SESSION[ 'login_user' ] = $myusername;

		header( "location: main.php" );
	} else {
		$error = "Your Login Name or Password is invalid";
	}
}
?>
<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<title>Book Stock</title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
	<link href="style.css" rel="stylesheet" type="text/css">
</head>

<body>
	<div class="container-fluid">
		<div class="d-flex justify-content-center">
			<h1>Book Management System</h1>
		</div>
		<div class="d-flex justify-content-center">
			<div class="col-md-3 mt-lg-5">
				<h3>Login</h3>
				<form action="" method="post">
					<div class="form-group">
						<input type="text" name="username" class="form-control" placeholder="Username" value=""/>
					</div>
					<div class="form-group">
						<input type="password" name="password" class="form-control" placeholder="Password" value=""/>
					</div>
					<div class="form-group">
						<input type="submit" class="btn btn-primary" value="Login"/>
					</div>
				</form>
				<div style="font-size:11px; color:#cc0000; margin-top:10px">
					<?php echo $error; ?>
				</div>
			</div>
		</div>
	</div>
</body>
</html>