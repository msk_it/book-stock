<?php
session_start();
require_once "dbconnection.php";

$id = $_GET[ 'id' ];

$sql = "SELECT b.Name AS Name, a.Name AS Author, b.Year AS Year, b.Price AS Price, b.ISBN AS ISBN, b.Medium AS Medium, g.Name AS Category FROM books b, author a, category g WHERE b.ISBN = '$id' AND b.Category = g.id AND b.Author = a.id";

if ( $result = mysqli_query( $db, $sql ) ) {

	if ( mysqli_num_rows( $result ) == 1 ) {

		$row = mysqli_fetch_array( $result, MYSQLI_ASSOC );


		$name = $row[ "Name" ];
		$author = $row[ "Author" ];
		$year = $row[ "Year" ];
		$price = $row[ "Price" ];
		$isbn = $row[ "ISBN" ];
		$medium = $row[ "Medium" ];
		$category = $row[ "Category" ];
	} else "<p class='lead'><em>No records were found.</em></p>";

} else {
	echo "ERROR: Could not able to execute $sql. " . mysqli_error( $db );
}

if ( $_SERVER[ "REQUEST_METHOD" ] == "POST" ) {
	$title = $_POST[ 'title' ];
	$author = $_POST[ 'author' ];
	$year = $_POST[ 'year' ];
	$price = $_POST[ 'price' ];
	$isbn = $_POST[ 'isbn' ];
	$medium = $_POST[ 'medium' ];
	$category = $_POST[ 'category' ];

	$sql = "UPDATE author SET Name = '$author' WHERE id = (Select Category from books where ISBN = '$isbn')";

	if ( mysqli_query( $db, $sql ) ) {
		echo( "Succsesfully update author" );
	} else {
		echo "Error: " . $sql . "<br>" . mysqli_error( $db );
	}

	$sql = "UPDATE books SET Name = '$title',  Year = '$year', Price = $price, Medium = '$medium', Category = '$category' WHERE ISBN = '$isbn'";

	if ( mysqli_query( $db, $sql ) ) {
		header( "location: main.php" );
		exit();
	} else {
		echo "Error: " . $sql . "<br>" . mysqli_error( $db );
	}

}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Update Record</title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.css">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.js"></script>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
	<link href="style.css" rel="stylesheet" type="text/css">
	<style type="text/css">
		.wrapper {
			width: 500px;
			margin: 0 auto;
			border: solid;
			border-color: #007bff;
			padding-bottom: 8px;
		}

	</style>
</head>

<body>
	<div class="wrapper">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="page-header">
						<h1>Update Record</h1>
					</div>
					<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
						<div class="form-group">
							<label>ISBN</label>
							<input type="text" name="isbn" class="form-control" readonly value="<?php echo $isbn; ?>">
						</div>
						<div class="form-group">
							<label>Title</label>
							<input name="title" type="text" class="form-control" value="<?php echo $name; ?>" required>
						</div>
						<div class="form-group">
							<label>Author</label>
							<input name="author" type="text" class="form-control" value="<?php echo $author; ?>" required>
						</div>
						<div class="form-group">
							<label>Year</label>
							<input name="year" type="number" class="form-control" value="<?php echo $year; ?>" required>
						</div>
						<div class="form-group">
							<label>Price</label>
							<input name="price" type="number" class="form-control" value="<?php echo $price; ?>" required>
						</div>
						<div class="form-group">
							<label>Medium</label>
							<input name="medium" type="text" class="form-control" value="<?php echo $medium; ?>" required>
						</div>
						<div class="form-group">
							<label>Category</label>
							<select class="form-control" name="category" required>
								<?php
								//include_once 'dbconnection.php';
								$sql = "SELECT * from category";
								$result = $db->query( $sql );
								if ( $result->num_rows > 0 ) {
									while ( $row = $result->fetch_assoc() ) {
										echo "<option value='" . $row[ 'id' ] . "'>" . $row[ 'Name' ] . "</option>";
									}
								}
								$db->close();
								?>
							</select>
						</div>
						<input type="submit" class="btn btn-primary" value="Update">
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>