<?php
include( 'session.php' );
require_once "dbconnection.php";

$error = "";

if($_SERVER["REQUEST_METHOD"] == "POST"){
	
	
	$isbn = $_POST[ 'isbn' ];
	$sql = "SELECT * FROM books WHERE ISBN = '$isbn'";
	$result = mysqli_query( $db, $sql );
	$row = mysqli_fetch_array( $result, MYSQLI_ASSOC );

	$count = mysqli_num_rows( $result );

	if ( $count > 0 ) {
		$error = "Sorry, ISBN Already Exists";
	} else {
	
	$target_dir = "images/";
	$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
	$uploadOk = 1;
	$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
	
	if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
	
	$title = $_POST['title'];
	$author = $_POST['author'];
	$year = $_POST['year'];
	$price = $_POST['price'];
	$isbn = $_POST['isbn'];
	$medium = $_POST['medium'];
	$image = $_FILES['fileToUpload']['name'];
	$category = $_POST['category'];
	
	$sql = "INSERT INTO author (Name) VALUES ('$author')";
	
	if (mysqli_query($db, $sql)) {
		echo("Succsesfully inserted author");
	} else {
		echo "Error: " . $sql . "<br>" . mysqli_error($db);
	}
	
	$sql = "INSERT INTO books (Name, Author, Year, Price, ISBN, Medium, Category, Image) VALUES ('$title', (Select id from author where Name = '$author' limit 1), $year, $price, '$isbn', '$medium', '$category', '$image')";
	
	if (mysqli_query($db, $sql)) {
		header("location: main.php");
                exit();
	} else {
		echo "Error: " . $sql . "<br>" . mysqli_error($db);
	}

	mysqli_close($db);
	}
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Add Book</title>
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.css">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.js"></script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
<link href="style.css" rel="stylesheet" type="text/css">
<script src="script.js" type="text/javascript"></script>
</head>
<body>
    <div class="wrapper1">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header">
                        <h2>Create Record</h2>
                    </div>
                    <p>Please fill this form and submit to add book to the database.</p>
                    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label>Title</label>
                            <input type="text" name="title" class="form-control" required>
						</div>
                        <div class="form-group">
                            <label>Author</label>
                            <input type="text" name="author" class="form-control" required>
                        </div>
						<div class="form-group">
                            <label>Year</label>
                            <input type="number" name="year" class="form-control" required>
                        </div>
						<div class="form-group">
                            <label>Price</label>
                            <input type="number" name="price" class="form-control" required>
                        </div>
						<div class="form-group">
                            <label>ISBN</label>
                            <input type="text" name="isbn" class="form-control" value="<?php echo $error?>" required>
                        </div>
						<div class="form-group">
                            <label>Medium</label>
                            <input type="text" name="medium" class="form-control" required>
                        </div>
						<div class="form-group">
                            <label>Image</label>
                            <input type="file" name="fileToUpload" id="fileToUpload" class="form-control">
                        </div>
						<div class="form-group">
                            <label>Category</label>
                           <select class="form-control" name="category" required>
							  <?php
								include_once 'dbconnection.php';
								$sql = "SELECT * from category";
								$result = $db->query( $sql );
								if ( $result->num_rows > 0 ) {
									while ( $row = $result->fetch_assoc() ) {
										echo "<option value='" . $row[ 'id' ] . "'>" . $row[ 'Name' ] . "</option>";
									}
								}
								$db->close();
								?>
							</select>
                        </div>
                        <input type="submit" class="btn btn-primary" value="Submit">
                        <a href="main.php" class="btn btn-default">Cancel</a>
                    </form>
                </div>
            </div>        
        </div>
    </div>
</body>
</html>